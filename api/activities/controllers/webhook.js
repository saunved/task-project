'use strict';

// The content manager itself doesn't have any hooks associated with it, 
// neither does it make any local API calls so 
// detecting a new activity with a webhook
module.exports = {
    afterCreation: async ctx => {
        // A new entry was added - verify if it was from the activities model
        if(ctx.request.body.model === 'activities'){
            try{
                // Use the strapi email plugin to send an email
                await strapi.plugins['email'].services.email.send({
                    to: 'info@mallorcard.es',
                    from: 'saunved.test1@gmail.com',
                    subject:  'New activity created',
                    text:  'A new activity was created on your app',
                    html:  `${new Date()}\n${JSON.stringify(ctx.request.body)}`
                  });
            }
            catch(err){
                console.log(err);
            }
            ctx.send('Ok');
        }
    }
}