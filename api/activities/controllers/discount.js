'use strict';
module.exports = {
    // PUT /activities_price
    index: async ctx => {

        let discount = parseInt(ctx.request.body.discount);
        
        // Ensure that discount is a valid number
        if(!Number.isNaN(discount)){

            try{
                // Get all activities
                let activities = await strapi.query('activities').find({});
                let updateCount = 0;

                // Update each activity with the discount, use for...of to ensure that the async, await works
                for (const activity of activities) {

                    // Ensure that price exists and is a number
                    if(!isNaN(activity.Price)){
                        await strapi.query('activities').update({_id: activity._id}, {
                            Price: activity.Price - (parseInt(discount)/100)*activity.Price 
                        })   
                        updateCount++;
                    }
                }
                ctx.send({message: 'success', updateCount: updateCount, totalCount: activities.length });
            }
            catch(error){
                ctx.send({message: 'error', error: 'An internal error occurred'})
            }
            
        }
        else{
            ctx.send({message: 'error', error: 'A valid discount value must be present'})
        }
    },
  };