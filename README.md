**Please note that I have used MongoDB.**
You can find the database dump in:
mongodump

You can restore the database by entering:
mongorestore -d palma mongodump

Strapi details -  
username: palma  
password: 12345678

All endpoints are public for this project. No auth is used, but that can be done if required.

**Task #1**  
I downloaded and installed Strapi v3.0.1 (later downgraded to v3.0.0) and set it up with MongoDB (MariaDB does not seem to be available). MongoDB is my preferred database unless there is a special requirement for another database (e.g. caching). 
I have setup MongoDB without any passwords (since we are locally developing)  
Time taken: 14 minutes

**Task #2, #3, #4**  
I created multilingual fields using Components, but I am surprised that there's no better way to group fields in Strapi. The existing solution works fine though.  
Time taken: 17 minutes

**Task #5**  
Creating the custom endpoint was fairly simple. I updated the routes.json file and linked it to a function in api/activities/controllers/discount.js  
Time taken: 34 minutes

**Task #6**  
I set up a webhook to detect when a new activity was created (and linked it to a route in the routes.json file, which links to the handler at api/activities/controllers/webhook.js). This part was simple, it took less than 10 minutes to setup.  

Setting up the automated email was the most time-consuming task because Strapi doesn't work like it used to before v3.0.0. Initially emails were setup from the UI, but now they are setup from the files. It took me some time to realize that since their documentation has not been updated in parts. I spent almost 2 hours trying to figure this out. I tried downgrading Strapi from v3.0.1 to v3.0.0. I tried installing different plugins. Finally I opened this issue on Github and got a link to the correct documentation:
[Link to the issue](https://github.com/strapi/strapi/issues/6519)

As you can see, a lot of people had the same issue between when I filed it and when I am writing this README:  
[Like this issue](https://github.com/strapi/strapi/issues/6527)  
[And this issue](https://github.com/strapi/strapi/issues/6528)

For the emails, I tried sending them with Nodemailer (as was suggested in the documentation), but that did not work. So I resorted to using SendGrid. **That worked.**
However, I personally prefer having only a SMTP relay without the frills that SendGrid offers and I was also curious as to why Nodemailer was not working.

So I opened the source code for the [nodemailer provider](https://github.com/yutikom/strapi-provider-email-nodemailer#readme) and tried modifying the files inside of node_modules itself by mapping them to how the sendgrid provider was coded.

After a fair bit of fiddling around, I reached a solution. I forked the existing plugin and created my own. I have published it to npm and it is [available here](https://www.npmjs.com/package/strapi-provider-email-nodemailer-v3). It works out of the box if you follow the README.  

I am using Gmail's SMTP server for sending the emails.

Time taken: 3 hours 55 minutes  
Time it should have taken (if the plugin worked): 25-35 minutes

